local t = 25
local function pause()
   mp.add_timeout(0.2, function() mp.osd_message("autopause", 2) end)
   mp.set_property_bool("pause", true)
end
timer = mp.add_timeout(t, pause)

local on_pause = function(_, paused)
   if not paused then
      timer:resume()
   else
      timer:kill()
   end
end

local function on_seeking(_, s)
   if s == true or s == nil then
      timer:stop()
   elseif s == false then
      timer:resume()
   end
end

mp.observe_property("seeking", "bool", on_seeking)
mp.observe_property("pause", "bool", on_pause)
