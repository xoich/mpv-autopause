local w = 120
local wt = 8
local timer

function warn()
   mp.osd_message("shutting down", 2)
   timer = mp.add_timeout(wt, function() mp.command("quit-watch-later") end)
end

local function start_cd()
   mp.osd_message("starting timer")
   timer = mp.add_timeout(w, warn)
end

local function abort()
   if timer then
	  mp.osd_message("timer aborted")
	  timer:kill()
   else
	  mp.osd_message("timer not started")
   end
end

mp.add_key_binding("k", nil, start_cd)
mp.add_key_binding("K", nil, abort)
